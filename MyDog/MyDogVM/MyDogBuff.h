#pragma once
#include "MyDogPool.h"
#include <vector>
using namespace std;
namespace MyDog {
	/// <summary>
	/// 用于封装二进制读取,本身不占什么内存的
	/// </summary>
	class ReadBuff {
		dptr data;
	public:
		ReadBuff(dptr d) { data = d; }
		~ReadBuff() { data = null; }

		template<typename T> ReadBuff& operator>>(T& t) {
			t = *(T*)data;
			data += sizeof(T);
			return *this;
		}
	};

	/// <summary>
	/// 非连续内存的dispbuff
	/// </summary>
	class WriteDispBuff {
		vector<BuffItem> vec;
		uint8 mApplyid;//内存不够时,固定申请
		uint32 mFixsize;//每个块的实际大小
		uint32 mLeft;//当前写入块剩余长度
		dptr getdata();
	public:
		WriteDispBuff(uint32 s = 128);
		~WriteDispBuff();
		/// <summary>
		/// 写入内存
		/// </summary>
		template<typename T> WriteDispBuff& operator<<(const T& t) {
			uint32 s = sizeof(T);
			uint32 off = 0;
			while (s>0) {
				dptr rdata = getdata();
				if (mLeft >= s) {
					memcpy(rdata, (dptr)(&t) + off, s);
					mLeft -= s;
					break;
				}
				else {
					memcpy(rdata,(dptr)(&t) + off, mLeft);
					mLeft = 0;
					off += mLeft;
					s -= mLeft;
				}
			}			
			return *this;
		}
		/// <summary>
		/// 写入内存
		/// </summary>
		WriteDispBuff& writeBuff(dptr data,uint32 s);
		/// <summary>
		/// fixs每块的大小
		/// useBlock用了几块
		/// left最后一块的使用长度
		/// </summary>
		inline void getInfo(uint32& fixs, uint16& useBlock, uint32& leng) {
			fixs = mFixsize;
			useBlock = vec.size();
			leng = mFixsize - mLeft;
		}
		/// <summary>
		/// 读取内存，和getInfo配合使用
		/// </summary>
		inline dptr readData(uint16 index) {
			return (dptr)vec[index].data;
		}
	};
}