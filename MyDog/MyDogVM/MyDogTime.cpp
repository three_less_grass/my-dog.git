#include "MyDogTime.h"
#include "MyDogFace.h"
namespace MyDog {
	extern stFrame gFrame;
	MyDogTimeCD::MyDogTimeCD(uint16 cd, bool sec):life(cd){
		endT = gFrame.shortTimeCB(sec) + cd;
	}
	void MyDogTimeCD::init(uint16 cd, bool sec) {
		life = cd;
		endT = gFrame.shortTimeCB(sec) + cd;
	}
////////////////////////////////////////////////////////////////////////////////////////////////
	/*MyDogTimeCellMs::MyDogTimeCellMs()
	:life(0),endT(0){
		func = null;
	}*/
////////////////////////////////////////////////////////////////////////////////////////////////
	/*MyDogTimeCell::MyDogTimeCell()
		:endT(0), life(0), vecType(0), vecPos(0), stop_state(0)
	{
		func = null;
	}*/

	MyDogTimerMgr::MyDogTimerMgr()
		: secid(0), minid(0), hourid(0), vecMs(), vecMsPool(), vecOthers(), vecPool(), vecTemp()
	{
		currSecT = gFrame.shortTimeCB(true);
		//vecSecs、vecMins、vecHours初始化
		for (int i = 0;i < 60;i++) {
			vecSecs[i].clear();
			vecMins[i].clear();
		}
		for (int i = 0; i < 24; i++) {
			vecHours[i].clear();
		}
	}
	MyDogTimerMgr::~MyDogTimerMgr()
	{
		
	}

	void MyDogTimerMgr::onLoop(uint32 nt) {
		//先执行毫秒级别的定时器
		uint32 msT = gFrame.shortTimeCB(false);
		EnumTimerResMs ms_res;
		MyDogTimeCellMs* tcms;
		for (int i = vecMs.size()-1;i>=0;i--) {
			tcms = vecMs[i];
			if (tcms->check(msT)) {
				ms_res = tcms->func();
				if (ms_res == EnumTimerResMs::comeon) {
					tcms->reset(msT);
				} else {
					//回收
					if (i != vecMs.size() - 1) {
						vecMs[i] = vecMs.back();
						vecMs[i]->id = i;
					}
					vecMs.pop_back();
					tcms->id = max_uint16;
					vecMsPool.push_back(tcms);
				}
			}
		}
		//执行秒
		auto dt = nt - currSecT;
		for (int i = 0; i < dt; i++) {
			runOneSec(nt);
		}
		currSecT = nt;
	}
	MyDogTimeCellMs* MyDogTimerMgr::addTime(TFuncMs func, uint16 ms) {
		uint32 msT = gFrame.shortTimeCB(false);
		MyDogTimeCellMs* cell;
		if (vecMsPool.empty()) {
			cell = new MyDogTimeCellMs();
		}
		else {
			cell = vecMsPool.back();
			vecMsPool.pop_back();
		}
		cell->func = func;
		cell->life = ms;
		cell->reset(msT);
		vecMs.push_back(cell);
		cell->id = vecMs.size() - 1;
		return cell;
	}
	void MyDogTimerMgr::stopTime(MyDogTimeCellMs* tcms) {
		uint16 s = vecMs.size()-1;
		if (tcms->id > s) {
			return;
		}
		if (vecMs[tcms->id] == tcms) {
			vecMs[tcms->id] = vecMs[s];
			vecMs.pop_back();
			vecMs[s]->id = tcms->id;
			tcms->id = max_uint16;
			vecMsPool.push_back(tcms);
		}
	}
	MyDogTimeCell* MyDogTimerMgr::addTime(TFunc func, uint32 sec)
	{
		uint32 secT = gFrame.shortTimeCB(true);
		auto cell = createCell();
		cell->func = func;
		cell->life = sec;
		cell->reset(secT);
		addCell(cell);
		return cell;
	}
	bool MyDogTimerMgr::setTime(MyDogTimeCell* tc, EnumTimerRes tr, uint32 currT)
	{
		//先找到tc所在vec,但是如果超时的则找不到
		if (!tc) return false;
		vector<MyDogTimeCell*>* vecPtr = null;
		if (tc->vecType == 0)
		{
			vecPtr = &vecSecs[tc->vecPos];
		}
		else if (tc->vecType == 1)
		{
			vecPtr = &vecMins[tc->vecPos];
		}
		else if (tc->vecType == 2)
		{
			vecPtr = &vecHours[tc->vecPos];
		}
		else if (tc->vecType == 3)
		{
			vecPtr = &vecOthers;
		}

		if (!vecPtr) return false;
		int id_ = 0; int s = vecPtr->size();
		if (s == 0 && tr != EnumTimerRes::comeon) return false;//没找到
		for (; id_ < s; id_++)
		{
			if (tc == (*vecPtr)[id_])
			{
				(*vecPtr)[id_] = null;//清空就好
				break;
			}
		}
		if (id_ == s && tr != EnumTimerRes::comeon) return false;//没找到

		if (tr == EnumTimerRes::comeon)
		{
			//重置时间并重新添加
			tc->reset(currT);
			addCell(tc);
		}
		else if (tr == EnumTimerRes::stop)
		{
			tc->onstop();
		}
		else if (tr == EnumTimerRes::hangup)
		{
			tc->onhangup(currT);
		}
		else if (tr == EnumTimerRes::release)
		{
			//回收
			freeCell(tc);
		}
		else
		{
			return false;
		}
		return true;
	}
	MyDogTimeCell* MyDogTimerMgr::createCell()
	{
		if (vecPool.empty())
		{
			return new MyDogTimeCell();
		}
		auto back = vecPool.back();
		vecPool.pop_back();
		return back;
	}
	void MyDogTimerMgr::addCell(MyDogTimeCell* cell)
	{
		auto sec = cell->life;
		if (cell->stop_state == 2)
		{
			sec = cell->endT;
			cell->stop_state = 0;
		}
		if (sec < 60)
		{
			//放在秒容器里
			auto id_ = (secid + sec - 1) % 60;
			auto vec = &vecSecs[id_];
			cell->vecType = 0;
			cell->vecPos = id_;
			vec->push_back(cell);
		}
		else if (sec < 3600)
		{
			//放在分容器里
			auto id_ = (minid + sec / 60 - 1) % 60;
			auto vec = &vecMins[id_];
			cell->vecType = 1;
			cell->vecPos = id_;
			vec->push_back(cell);
		}
		else if (sec < 86400)
		{
			//放在时容器里
			auto id_ = (hourid + sec / 3600 - 1) % 24;
			auto vec = &vecHours[id_];
			cell->vecType = 2;
			cell->vecPos = id_;
			vec->push_back(cell);
		}
		else
		{
			//超过一天就只能放在剩余容器里
			cell->vecType = 3;
			vecOthers.push_back(cell);
		}
	}
	void MyDogTimerMgr::freeCell(MyDogTimeCell* cell)
	{
		vecPool.push_back(cell);
	}
	void MyDogTimerMgr::runOneSec(uint32 currT)
	{
		//重新添加回定时器的列表
		vector<MyDogTimeCell*>& revec = vecTemp;
		//把当前所有cell都do一遍
		auto vec = &vecSecs[secid];
		MyDogTimeCell* currCell = null;
		EnumTimerRes res;
		//有可能func的时候添加cell，从而增加vec的长度 
		for (int i = 0; i < vec->size(); i++)
		{
			currCell = (*vec)[i];
			if (!currCell)
			{
				continue;
			}
			//check cd
			if (currCell->check(currT))
			{
				res = currCell->func();
				switch (res)
				{
				case EnumTimerRes::comeon:
				{
					currCell->reset(currT);
					revec.push_back(currCell);
				}
				break;
				case EnumTimerRes::stop:
				{
					currCell->onstop();
				}
				break;
				case EnumTimerRes::hangup:
				{
					currCell->onhangup(currT);
				}
				break;
				case EnumTimerRes::release:
				{
					freeCell(currCell);
				}
				break;
				}
			}
			else
			{
				//报警todo
				revec.push_back(currCell);
			}
		}
		vec->clear();
		secid++;
		if (secid >= 60)//下一秒刚好过一分钟
		{
			secid = 0;
			auto vecMinPtr = &vecMins[minid];
			//merge(revec.begin(), revec.end(), vecMinPtr->begin(), vecMinPtr->end(), back_inserter(revec));
			revec.insert(revec.end(), vecMinPtr->begin(), vecMinPtr->end());
			vecMinPtr->clear();
			minid++;
			if (minid >= 60)
			{
				minid = 0;
				auto vecHourPtr = &vecHours[hourid];
				revec.insert(revec.end(), vecHourPtr->begin(), vecHourPtr->end());
				vecHourPtr->clear();
				hourid++;
				if (hourid >= 24)
				{
					hourid = 0;
					//把剩余容器处理掉
					revec.insert(revec.end(), vecOthers.begin(), vecOthers.end());
					vecOthers.clear();
				}
			}
		}

		//重新设定timer
		for (auto cell : revec)
		{
			addCell(cell);
		}
		revec.clear();
	}
}