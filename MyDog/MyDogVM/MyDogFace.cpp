#include <iostream>
#include "MyDogFace.h"
#include "MyDogEntityMgr.h"
#include "MyDogVM.h"
#include "MyDogEvent.h"
#include "MyDogPool.h"
#include "MyDogTime.h"
#include "MyDogCom.h"
#include "MyDogBuff.h"
#include <stdarg.h>

using namespace std;

MYDOG_EXTERN_GLOBAL

namespace MyDog {
	stFrame gFrame;
	bool gMainInit(stFrame& frame) {
		gFrame = frame;
		if (gFrame.readCB == nullptr) {
			return false;
		}
		//读取启动配置
		char* data = (char*)(gFrame.readCB(gFrame.confFile, 0));
		if (data == nullptr) {
			return false;
		}
		ReadBuff rb(data);
		rb >> gAccuracy;
		gEntityMgr = new MyDogEntityMgr(rb);
		gVM = new MyDogVM(rb);
		gEvtMgr = new MyDogEventMgr(gVM);
		gPoolMgr = new MyDogPoolMgr(rb);
		gTimerMgr = new MyDogTimerMgr();
		return true;
	}
	int gOnMsg(unsigned char msgType, unsigned char msgSubType, const char* buff, int leng) {
		return gEvtMgr->OnMsg(msgType, msgSubType, buff, leng);
	}
	void gLoop() {
		auto nt = gFrame.shortTimeCB(true);
		gTimerMgr->onLoop(nt);
		gPoolMgr->onLoop(nt);
	}
	void gExit() {

	}
	void gLog(ELogType lt, const char* fmt, ...) {
		if (!gFrame.logCB) {
			return;
		}
		va_list p;
		va_start(p, fmt);

		static char out[1024];
		int ret = vsprintf(out, fmt, p); // 把可变参数内容都以字符串形式，写入out
		if (ret != -1)
		{
			out[1023] = 0;
			gFrame.logCB(out,lt);
		}
		va_end(p);
	}


	//int vasprintf(char** strp, const char* fmt, va_list ap) {
	//	va_list ap2;
	//	va_copy(ap2, ap);
	//	char tmp[1];
	//	int size = vsnprintf(tmp, 1, fmt, ap2);
	//	if (size <= 0) return size;
	//	va_end(ap2);
	//	size += 1;
	//	*strp = (char*)malloc(size * sizeof(char));
	//	return vsnprintf(*strp, size, fmt, ap);
	//}
	//void gLog(ELogType lt, const char* fmt, ...) {
	//	if (!gFrame.logCB) {
	//		return;
	//	}
	//	va_list p;
	//	va_start(p, fmt);

	//	// 接口 vasprintf，是专门做格式解析工作的，我们这里目的是可变参数，格式解析用接口带过
	//	char* out;
	//	int ret = vasprintf(&out, fmt, p); // 把可变参数内容都以字符串形式，写入out
	//	if (ret != -1)
	//	{
	//		gFrame.logCB(out,lt);
	//		free(out);	// 手动释放动态开辟空间
	//	}
	//	va_end(p);
	//}
}