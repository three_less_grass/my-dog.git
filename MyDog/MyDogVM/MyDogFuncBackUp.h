#pragma once
#include "MyDogStack.h"
namespace MyDog {
	extern thread_local MyDogPoolMgr* gPoolMgr;
	/// <summary>
	/// 用于脚本函数的的挂起和继续
	/// </summary>
	class MyDogFuncBackUp {
		funcid fid;
		//MyDogStack的数据备份
		BuffItem item; uint16 itemLeng; MDRegister* currR; MDRegister* endStackR; uint16 leftLeng;
	public:
		/// <summary>
		/// 备份Stack的数据
		/// </summary>
		template<uint16 StackLeng, uint16 HeapLeng>
		MyDogFuncBackUp& operator <<(MyDogStack<StackLeng, HeapLeng>& stack) {
			currR = stack.mCurrR;
			endStackR = stack.mEndStackR;
			leftLeng = stack.leftLeng;
			itemLeng = 0;
			if (endStackR) {
				itemLeng = endStackR->bPtr-stack.mData + endStackR->leng;
				item = gPoolMgr->apply(itemLeng);
				memcpy(item.data, stack.mData, itemLeng);
			}
			return *this;
		}
		/// <summary>
		/// 还原MyDogStack的数据
		/// </summary>
		template<uint16 StackLeng, uint16 HeapLeng>
		MyDogFuncBackUp& operator >>(MyDogStack<StackLeng, HeapLeng>& stack) {
			stack.mCurrR = currR;
			stack.mEndStackR = endStackR;
			stack.leftLeng = leftLeng;
			if (itemLeng>0) {
				memcpy(stack.mData, item.data, itemLeng);
			}
			//重设寄存器指针
			auto dPtr = stack.mData + itemLeng;
			while (endStackR) {
				dPtr -= endStackR->leng;
				endStackR->bPtr = dPtr;
				endStackR = endStackR->last;
			}
			return *this;
		}
	};
}
