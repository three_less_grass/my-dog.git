#pragma once
#include "MyDogCom.h"
#include "MyDogTime.h"
namespace MyDog {
	/// <summary>
	/// 申请固定大小的内存池
	/// 内部使用类,不考虑那些安全隐患的地方
	/// 比如不用考虑归还的内存是否长度对应等
	/// </summary>
	class MyDogPoolFixSize
	{
		uint16 mNum;
		int32 fixsize;
		void* mPool;
		MyDogTimeCD timecd;
	public:
		/// <summary>
		/// fixs为固定内存大小
		/// 最小为sizeof(void*) + 1
		/// </summary>
		MyDogPoolFixSize(int32 fixs,uint16 cd);
		~MyDogPoolFixSize();
		/// <summary>
		/// 创建cnt个内存放入池里
		/// </summary>
		void create(int32 cnt = 1);
		/// <summary>
		/// 申请内存
		/// </summary>
		vptr apply();
		/// <summary>
		/// 回收内存
		/// </summary>
		void free(vptr data);
		/// <summary>
		/// cd检测
		/// </summary>
		inline bool checkCd(uint32 nt) {
			return timecd.check(nt);
		}
		/// <summary>
		/// releasePro是每次release时的释放比例(百分比)
		/// </summary>
		void release(int8 releasePro = 60);
	};

#pragma pack(push)
#pragma pack(1)
	struct PoolConfItem {
		uint32 fixsize;//用于MyDogPoolFixSize初始化
		uint8 releasePro;//每次release时的释放比例(百分比)
		uint16 releaseCD;//释放cd 秒
	};
#pragma pack(pop)

	struct BuffItem {
		vptr data;
		uint8 index;//max_uint8表示超出配置
		BuffItem():data(null),index(max_uint8){}
		BuffItem(vptr d,int i):data(d),index(i){}
		~BuffItem() { data = nullptr; }
	};


	class ReadBuff;
	/// <summary>
	/// 管理所有的MyDogPoolFixSize
	/// 由于主要服务于属性集,所以可以在编译前知道各种属性集大小,从而得出PoolConfItem
	/// </summary>
	class MyDogPoolMgr {
		PoolConfItem* pool_items;
		MyDogPoolFixSize** mPool;
		uint8 mLeng;
		MyDogTimeCD onesecCd;
	public:
		MyDogPoolMgr(ReadBuff& rb);
		~MyDogPoolMgr();
		/// <summary>
		/// 1、根据大小，获取index
		/// 2、rs为内存块的真实大小
		/// 3、如果返回值是max_uint8，那么就不能调用applyByid申请内存
		/// 而是调用applyBysize.这样牺牲了便利性，所以有了apply函数
		/// </summary>
		uint8 s2index(uint32 s, uint32* rs=null);
		/// <summary>
		/// 获取配置最大index
		/// </summary>
		inline uint8 maxindex() {
			return mLeng - 1;
		}
		/// <summary>
		/// 根据index申请一段内存
		/// </summary>
		BuffItem applyByid(uint8 index);
		/// <summary>
		/// 申请超出池配置的内存
		/// </summary>
		BuffItem applyBysize(uint32 s);
		/// <summary>
		/// 将applyByid和applyBysize结合起来的便利函数
		/// </summary>
		inline BuffItem apply(uint8 index, uint32 s) {
			if (index == max_uint8) {
				return applyBysize(s);
			}
			return applyByid(index);
		}
		inline BuffItem apply(uint32 s) {
			uint8 index = s2index(s);
			if (index == max_uint8) {
				return applyBysize(s);
			}
			return applyByid(index);
		}
		/// <summary>
		/// 归还内存
		/// </summary>
		void replay(BuffItem& item);
		void replay(vptr data, uint8 index);
		/// <summary>
		/// 循环调用
		/// </summary>
		void onLoop(uint32 nt);
	};
}
