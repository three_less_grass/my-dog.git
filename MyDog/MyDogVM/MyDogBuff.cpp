#include "MyDogBuff.h"

namespace MyDog { 
	extern thread_local MyDogPoolMgr* gPoolMgr;

	WriteDispBuff::WriteDispBuff(uint32 s)
		:vec(), mFixsize(s), mLeft(0){
		mApplyid = gPoolMgr->s2index(s,&mFixsize);
		if (mApplyid == max_uint8) {
			mApplyid = gPoolMgr->maxindex();
		}
	}
	WriteDispBuff::~WriteDispBuff() {
		for (auto& it:vec) {
			gPoolMgr->replay(it);
		}
		vec.clear();
	}
	dptr WriteDispBuff::getdata() {
		if (mLeft <= 0) {
			auto it = gPoolMgr->applyByid(mApplyid);
			vec.push_back(it);
			mLeft = mFixsize;
		}
		auto& it = vec.back();
		return ((dptr)it.data) + (mFixsize - mLeft);
	}
	WriteDispBuff& WriteDispBuff::writeBuff(dptr data, uint32 s) {
		uint32 off = 0;
		while (s>0) {
			dptr rdata = getdata();
			if (mLeft >= s) {
				memcpy(rdata, data + off, s);
				mLeft -= s;
				break;
			}
			else {
				memcpy(rdata, data + off, mLeft);
				mLeft = 0;
				off += mLeft;
				s -= mLeft;
			}
		}
		return *this;
	}
}