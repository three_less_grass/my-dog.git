#pragma once
#ifdef MYDOG_GATHER

#include <unordered_map>
#include <string>
#include "MyDogCom.h"
using namespace std;
namespace MyDog {
#pragma pack(push)
#pragma pack(1)
	union GatherData{
		int cnt;

	};
#pragma pack(pop)
	/// <summary>
	/// 数据收集,内部使用
	/// </summary>
	class MyDogGather
	{
	public:
		enum ETitle {
			mem_apply,//内存申请
			end,
		};
		static MyDogGather* Ins() {
			static MyDogGather* mIns = new MyDogGather();
			return mIns;
		}
	protected:
		MyDogGather();
		~MyDogGather();		
		unordered_map<string, GatherData*> mHash[end];
	};
}

#endif // MYDOG_GATHER
