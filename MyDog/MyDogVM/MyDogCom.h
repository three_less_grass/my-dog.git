#pragma once
namespace MyDog {
	typedef char int8;
	typedef unsigned char uint8;
	typedef short int16;
	typedef unsigned short uint16;
	typedef int int32;
	typedef unsigned int uint32;
	typedef long long int64;
	typedef unsigned long long uint64;
	typedef void* vptr;
	typedef char* dptr;
	typedef unsigned int funcid;

	constexpr auto null = nullptr;
	constexpr uint8 max_uint8 = 0xFF;
	constexpr uint16 max_uint16 = 0xFFFF;
}
#define Delete(ptr) if(ptr){delete ptr;ptr = null;}
#define DeleteArray(ptr) if(ptr){delete []ptr;ptr = null;}

//声明所有全局变量

#define MYDOG_EXTERN_GLOBAL namespace MyDog {\
extern int gAccuracy;\
extern thread_local MyDogEntityMgr* gEntityMgr;\
extern thread_local MyDogVM* gVM;\
extern thread_local MyDogPoolMgr* gPoolMgr;\
extern thread_local MyDogTimerMgr* gTimerMgr;\
extern thread_local MyDogEventMgr* gEvtMgr;\
}\

