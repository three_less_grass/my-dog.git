#pragma once



//宿主框架读取文件接口
typedef void* (__stdcall* ReadCallBack)(const char* file, int reason);
//宿主框架写入文件接口
typedef void (__stdcall* WriteCallBack)(const char* file, const char* data, int size, int reason);
//宿主框架短时间接口(秒-系统短时间戳 或 毫秒-进程运行的时间)
typedef unsigned int(__stdcall* ShortTimeCallBack)(bool sec);
//宿主框架提供的log接口
typedef void (__stdcall* LogCallBack)(const char* logstr, int logtype);

#pragma pack(push)
#pragma pack(4)
/// <summary>
/// 这里定义框架提供的回调以及初始化配置等数据
/// </summary>
struct stFrame {
	char confFile[64];
	ReadCallBack readCB;
	WriteCallBack writeCB;
	ShortTimeCallBack shortTimeCB;
	LogCallBack logCB;
};
#pragma pack(pop)

namespace MyDog{
	//这里实现对外接口
	bool gMainInit(stFrame& frame);
	int gOnMsg(unsigned char msgType, unsigned char msgSubType,const char* buff,int leng);
	void gLoop();
	void gExit();
	enum ELogType {
		normal,
		warning,
		error,
	};
	void gLog(ELogType lt, const char* fmt, ...);
}

#define MDLog(lt,fmt,...) gLog(lt,"[%s:%d]"##fmt, __FILE__, __LINE__, ##__VA_ARGS__);
