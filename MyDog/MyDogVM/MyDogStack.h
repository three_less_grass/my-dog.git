#pragma once
#include "MyDogCom.h"
#include "MyDogPool.h"
#include "MyDogFlag.h"

namespace MyDog {
	enum EMDRegflag {
		instack = 0x1,//是否属于栈
		replay = 0x2,//是否需要回归
	};
#pragma pack(push)
#pragma pack(1)
	/// <summary>
	/// 逻辑上的寄存器,一个函数对应一个寄存器
	/// 这个是对应lua的寄存器，只是大部分情况下是由栈组成
	/// 因为每个函数的Register最大值都是可以在编译时就确认的，所以不用关心越界的问题
	/// MDRegister本身也是从池里获取的
	/// </summary>
	struct MDRegister {
		dptr bPtr;
		uint16 leng;
		MDFlag<1> flag;//EMDRegflag
		MDRegister* last;
		/// <summary>
		/// 初始化
		/// </summary>
		inline void init(dptr data, uint16 l, int f, MDRegister* lastItem) {
			bPtr = data;
			leng = l;
			flag = f;
			last = lastItem;
		}
		/// <summary>
		/// 是否堆上第一个寄存器
		/// </summary>
		inline bool isBeginHeap() {
			//if ((!flag.get(0)) && flag.get(1)) {
			if (flag.get(1)) {
				return (!last) || (last->flag.get(0));
			}
			return false;
		}
	};
#pragma pack(pop)

	extern thread_local MyDogPoolMgr* gPoolMgr;
	//class MyDogFuncBackUp;
	/// <summary>
	/// 栈优先的堆栈，不连续的内存
	/// 脚本里的输入输出，临时数据都存在这里
	/// </summary>
	template<uint16 StackLeng,uint16 HeapLeng = StackLeng>
	class MyDogStack {
	protected:
		friend class MyDogFuncBackUp;
		int8 mData[StackLeng];
		uint16 leftLeng;//还剩多少内存
		MDRegister* mCurrR;//当前使用的寄存器
		MDRegister* mEndStackR;//为了方便备份
		uint8 mHeapIndex;//用于申请堆内存
	public:
		MyDogStack():leftLeng(StackLeng),mCurrR(null), mEndStackR(null){
			mHeapIndex = gPoolMgr->s2index(HeapLeng);
		}
		inline static uint8 getRegIndex() {
			static uint8 regindex = gPoolMgr->s2index(sizeof(MDRegister));
			return regindex;
		}
		inline static MDRegister* createReg() {
#ifdef MYDOG_CHECK
			auto buffit = gPoolMgr->apply(getRegIndex(), sizeof(MDRegister));
#else
			auto buffit = gPoolMgr->applyByid(getRegIndex());
#endif // MYDOG_CHECK

			
			return (MDRegister*)buffit.data;
		}
		/// <summary>
		/// 当前寄存器
		/// </summary>
		inline MDRegister* getCurrReg() {
			return mCurrR;
		}
		/// <summary>
		/// 归还寄存器
		/// </summary>
		static void replayReg(MDRegister* reg) {
			if (reg->isBeginHeap()) {
				//归还堆内存
				gPoolMgr->replay(reg->bPtr, mHeapIndex);
			}
			gPoolMgr->replay(reg, getRegIndex());
		}
		/// <summary>
		/// 申请寄存器
		/// </summary>
		MDRegister* applyReg(uint16 leng) {
			//从池中取一个没初始化的寄存器			
			MDRegister* newR = createReg();
			dptr data; int flag = EMDRegflag::instack;
			if (leftLeng < leng) {
				//需要申请堆内存
				flag = EMDRegflag::replay;
				auto buffit = gPoolMgr->apply(mHeapIndex, HeapLeng);
				data = (dptr)buffit.data;
				leftLeng = HeapLeng;
			}
			else {
				if (mCurrR) {
					data = mCurrR->bPtr + mCurrR->leng;
					if (!mCurrR->flag.get(0)) {
						//堆上
						flag = 0;
					}
					else {
						//栈上
						data = mData;
						mEndStackR = newR;
					}
				}
				else {
					//栈上
					data = mData;
					mEndStackR = newR;
				}
				leftLeng -= leng;				
			}
			newR->init(data, leng, flag, mCurrR);
			mCurrR = newR;
			return mCurrR;
		}
	};
}