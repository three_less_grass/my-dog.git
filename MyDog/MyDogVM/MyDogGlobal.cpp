#pragma once
#include "MyDogEntityMgr.h"
#include "MyDogVM.h"
#include "MyDogPool.h"
#include "MyDogTime.h"
#include "MyDogEvent.h"

namespace MyDog {
	///线程共享的全局变量
	int gAccuracy = 0;

	///线程本地的全局变量
	thread_local MyDogEntityMgr* gEntityMgr = nullptr;
	thread_local MyDogVM* gVM = nullptr;
	thread_local MyDogPoolMgr* gPoolMgr = nullptr;
	thread_local MyDogTimerMgr* gTimerMgr = nullptr;
	thread_local MyDogEventMgr* gEvtMgr = nullptr;
}