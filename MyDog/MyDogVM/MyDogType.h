#pragma once
namespace MyDog {
	extern int gAccuracy;
#pragma pack(push)
#pragma pack(1)

	///为了解决浮点数精度，和网络数据不一致的问题,采用整数存储
#pragma region 自定义浮点数

// == != < <= > >=
#define OPER_FUN_BOOL(Symbol,Type) inline bool operator Symbol(const Type& mdt){\
	return data Symbol mdt.data;\
	}\
// + -  
#define OPER_FUN_MD(Symbol,Type) inline Type operator Symbol(const Type& mdt){\
	Type res;\
	res.data = data Symbol mdt.data;\
	return res;\
	}\
// = += -=
#define OPER_FUN_MDREF(Symbol,Type) inline Type& operator Symbol(const Type& mdt){\
	data Symbol mdt.data; \
	return *this;\
	}\
//
#define OPER_FUN_OTHER(Symbol,Type1,Type2) inline Type1 operator Symbol(const Type2& rdata) {\
	Type1 res;\
	res.data = data Symbol (rdata * gAccuracy);\
	return res;\
	}\
//
#define MDF_MDD(Symbol,Type) static inline MDdouble operator Symbol(MDfloat left, const Type& rdata) {\
	MDdouble res(left);\
	res Symbol= rdata;\
	return res;\
	}\

	struct MDdouble;
	struct MDfloat {
	protected:
		int data;
	public:
		MDfloat(double d) {
			data = d * gAccuracy;
		}
		MDfloat(float d) {
			data = d * gAccuracy;
		}
		MDfloat(int d = 0) {
			data = d * gAccuracy;
		}
		MDfloat(char d) {
			data = d * gAccuracy;
		}
		MDfloat(long long d) {
			data = d * gAccuracy;
		}
		MDfloat(const MDdouble& d);

		OPER_FUN_BOOL(==,MDfloat)
		OPER_FUN_BOOL(!=, MDfloat)
		OPER_FUN_BOOL(<,MDfloat)
		OPER_FUN_BOOL(<=, MDfloat)
		OPER_FUN_BOOL(>, MDfloat)
		OPER_FUN_BOOL(>=, MDfloat)

		OPER_FUN_OTHER(+, MDfloat, int)
		OPER_FUN_OTHER(-, MDfloat, int)
		inline MDfloat operator *(const int& rdata) {
			MDfloat res;
			res.data = data * rdata;
			return res;
		}
		inline MDfloat operator /(const int& rdata) {
			if (rdata == 0) {
				throw "MDfloat rdata div 0";
			}
			MDfloat res; 
			res.data = data / rdata;
			return res;
		}

		OPER_FUN_MD(+, MDfloat);
		OPER_FUN_MD(-, MDfloat);
		inline MDfloat operator *(const MDfloat& mdt) {
			MDfloat res;
			res.data = data * mdt.data / gAccuracy;
			return res;
		}
		inline MDfloat operator /(const MDfloat& mdt) {
			if (mdt.data == 0) {
				throw "MDfloat div 0";
			}
			return MDfloat(data / mdt.data);
		}
		
		OPER_FUN_MDREF(=, MDfloat)
		OPER_FUN_MDREF(+=, MDfloat)
		OPER_FUN_MDREF(-=, MDfloat)
		inline MDfloat& operator *=(const MDfloat& mdt) {
			data = data * mdt.data / gAccuracy;
			return *this;
		}
		inline MDfloat& operator /=(const MDfloat& mdt) {
			if (mdt.data == 0) {
				throw "MDfloat div 0";
			}
			data = data / mdt.data * gAccuracy;
			return *this;
		}
		inline operator float() {
			return data / (float)gAccuracy;
		}
		inline int getdata() const {
			return data;
		}
	};
	struct MDdouble {
	protected:
		long long data;
	public:
		MDdouble(double d) {
			data = d * gAccuracy;
		}
		MDdouble(float d) {
			data = d * gAccuracy;
		}
		MDdouble(int d = 0) {
			data = d * gAccuracy;
		}
		MDdouble(char d) {
			data = d * gAccuracy;
		}
		MDdouble(long long d) {
			data = d * gAccuracy;
		}
		MDdouble(MDfloat d) {
			data = d.getdata();
		}

		OPER_FUN_BOOL(== , MDdouble)
		OPER_FUN_BOOL(!= , MDdouble)
		OPER_FUN_BOOL(< , MDdouble)
		OPER_FUN_BOOL(<= , MDdouble)
		OPER_FUN_BOOL(> , MDdouble)
		OPER_FUN_BOOL(>= , MDdouble)

		OPER_FUN_OTHER(+, MDdouble, int)
		OPER_FUN_OTHER(-, MDdouble, int)
		inline MDdouble operator *(const int& rdata) {
			MDdouble res;
			res.data = data * rdata;
			return res;
		}
		inline MDdouble operator /(const int& rdata) {
			if (rdata == 0) {
				throw "MDdouble rdata div 0";
			}
			MDdouble res;
			res.data = data / rdata;
			return res;
		}

		OPER_FUN_OTHER(+, MDdouble, long long)
		OPER_FUN_OTHER(-, MDdouble, long long)
		inline MDdouble operator *(const long long& rdata) {
			MDdouble res;
			res.data = data * rdata;
			return res;
		}
		inline MDdouble operator /(const long long& rdata) {
			if (rdata == 0) {
				throw "MDdouble rdata div 0";
			}
			MDdouble res;
			res.data = data / rdata;
			return res;
		}

		OPER_FUN_MD(+, MDdouble);
		OPER_FUN_MD(-, MDdouble);
		inline MDdouble operator *(const MDdouble& mdt) {
			MDdouble res;
			res.data = data * mdt.data / gAccuracy;
			return res;
		}
		inline MDdouble operator /(const MDdouble& mdt) {
			if (mdt.data == 0) {
				throw "MDdouble div 0";
			}
			return MDdouble(data / mdt.data);
		}

		OPER_FUN_MDREF(=, MDdouble);
		OPER_FUN_MDREF(+=, MDdouble);
		OPER_FUN_MDREF(-=, MDdouble);
		inline MDdouble& operator *=(const MDdouble& mdt) {
			data = data * mdt.data / gAccuracy;
			return *this;
		}
		inline MDdouble& operator /=(const MDdouble& mdt) {
			if (mdt.data == 0) {
				throw "MDdouble div 0";
			}
			data = data / mdt.data * gAccuracy;
			return *this;
		}

		inline operator double() {
			return data /(double)gAccuracy;
		}
		inline long long getdata() const {
			return data;
		}
	};

	
	//MDfloat遇到long long,要转成MDdouble处理
	MDF_MDD(+, long long);
	MDF_MDD(-, long long);
	MDF_MDD(*, long long);
	MDF_MDD(/, long long);
#pragma endregion
#pragma region int128/uint128 以后再实现todo
	struct MDint128 {

	};
	struct MDuint128 {

	};
#pragma endregion

#pragma pack(pop)

}