#pragma once
#include "MyDogCom.h"
#include <unordered_map>

using namespace std;
namespace MyDog {
	class MyDogVM;
	/// <summary>
	/// 包括了各种事件的管理
	/// </summary>
	class MyDogEventMgr
	{
		MyDogVM* mVM;
		unordered_map<int16, funcid> mHashMsg;
	public:
		MyDogEventMgr(MyDogVM* vm);
		~MyDogEventMgr();
		/// <summary>
		/// 注册逻辑消息
		/// </summary>
		inline void RegMsg(uint8 msgType,uint8 msgSubType, funcid id);
		/// <summary>
		/// 收到消息
		/// </summary>
		int OnMsg(uint8 msgType, uint8 msgSubType, const char* buff, int leng);
	};
}
