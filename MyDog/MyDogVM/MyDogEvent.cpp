#include "MyDogEvent.h"
#include "MyDogVM.h"
#include "MyDogFace.h"

namespace MyDog {
	MyDogEventMgr::MyDogEventMgr(MyDogVM* vm) :mVM(vm){

	}
	MyDogEventMgr::~MyDogEventMgr() {

	}
	inline void MyDogEventMgr::RegMsg(uint8 msgType, uint8 msgSubType, funcid id) {
#ifdef MYDOG_CHECK
		if (mHashMsg.find((msgType << 8) + msgSubType) != mHashMsg.end()) {
			MDLog(ELogType::warning,"reg same msg key,%d,%d", (int)msgType, (int)msgSubType);
		}
#endif
		mHashMsg[(msgType << 8) + msgSubType] = id;
	}
	int MyDogEventMgr::OnMsg(uint8 msgType, uint8 msgSubType, const char* buff, int leng) {
		auto it = mHashMsg.find((msgType << 8) + msgSubType);
		if (it == mHashMsg.end()) {
			MDLog(ELogType::warning, "not reg msg key,%d,%d", (int)msgType, (int)msgSubType);
			return 1;
		}		
		return mVM->Run(it->second,buff,leng);
	}
}