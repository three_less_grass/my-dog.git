#include "MyDogPool.h"
#include "malloc.h"
#include "MyDogBuff.h"
#include "MyDogFace.h"

namespace MyDog {
	extern stFrame gFrame;

	MyDogPoolFixSize::MyDogPoolFixSize(int32 fixs, uint16 cd):timecd(cd) {
		if (fixs < sizeof(vptr) + 1) {
			fixs = sizeof(vptr) + 1;
		}
		fixsize = fixs;
		mPool = null;
		mNum = 0;
	}
	MyDogPoolFixSize::~MyDogPoolFixSize() {
		release(100);
	}
	void MyDogPoolFixSize::create(int32 cnt) {
		for (int i = 0;i<cnt;i++) {
			vptr item = malloc(fixsize);
			//把item放到mPool前面
			*(vptr*)item = mPool;
			mPool = item;
		}
		mNum += cnt;
	}
	vptr MyDogPoolFixSize::apply() {
		if (!mPool) {
			create();
		}
		vptr buff = mPool;
		mPool = *(vptr*)mPool;
		mNum--;
		return buff;
	}
	void MyDogPoolFixSize::free(vptr data) {
		*(vptr*)data = mPool;
		mPool = data;
		mNum++;
	}
	void MyDogPoolFixSize::release(int8 releasePro) {
		if (!mPool) {
			return;
		}
		int free_cnt = releasePro * mNum / 100;
		if (free_cnt > mNum) {
			free_cnt = mNum;
		}
		vptr next = *(vptr*)mPool;
		for (int i = 0;i<free_cnt;i++) {
			free(mPool);
			mPool = next;
			if (!mPool) {
				return;
			}	
			next = *(vptr*)mPool;
		}
	}

///////////////////////////////////////////////////////////////////////////////////
	MyDogPoolMgr::MyDogPoolMgr(ReadBuff& rb){
		uint16 checkcd = 1;
		rb >> checkcd >> mLeng ;
		onesecCd.init(checkcd);
		pool_items = new PoolConfItem[mLeng];		
		for (int i = 0; i < mLeng; i++) {
			rb >> pool_items[i];
		}
		mPool = new MyDogPoolFixSize * [mLeng];
		memset(mPool, 0, sizeof(MyDogPoolFixSize*)* mLeng);
	}
	MyDogPoolMgr::~MyDogPoolMgr() {
		Delete(pool_items);
		for (int i = 0;i< mLeng;i++) {
			if (mPool[i]) {
				Delete(mPool[i]);
			}
		}
		DeleteArray(mPool);
	}
	uint8 MyDogPoolMgr::s2index(uint32 s, uint32* rs) {
		PoolConfItem* conf = &pool_items[mLeng - 1];
		if (s > conf->fixsize) {
			//申请的大于配置
			MDLog(ELogType::warning, "s2index [%d] > [%d]", s, conf->fixsize);
			if (rs) {
				*rs = s;
			}
			return max_uint8;
		}
		//可以通过缓存结果进一步优化的。。。todo
		for (uint8 i = 0; i < mLeng; i++) {
			conf = &pool_items[i];
			if (s <= conf->fixsize) {
				if (rs) {
					*rs = conf->fixsize;
				}
				return i;
			}
		}
		return max_uint8;
	}
	BuffItem MyDogPoolMgr::applyByid(uint8 index) {
		if (index == max_uint8) {
			return BuffItem(null, index);
		}
		auto pool = mPool[index];
		if (!pool) {
			auto& conf = pool_items[index];
			pool = new MyDogPoolFixSize(conf.fixsize, conf.releaseCD);
			mPool[index] = pool;
		}
		return BuffItem(pool->apply(), index);
	}
	BuffItem MyDogPoolMgr::applyBysize(uint32 s) {
		return BuffItem(malloc(s), max_uint8);
	}
	void MyDogPoolMgr::replay(BuffItem& item) {
		if (!item.data) {
			return;
		}
		auto index = item.index;
		if (index>= mLeng) {
			free(item.data);
			item.data = null;
			return;
		}
		auto pool = mPool[index];
		if (!pool) {
			auto& conf = pool_items[index];
			pool = new MyDogPoolFixSize(conf.fixsize, conf.releaseCD);
			mPool[index] = pool;
		}
		pool->free(item.data);
		item.data = null;
	}
	void MyDogPoolMgr::replay(vptr data, uint8 index) {
		if (!data) {
			return;
		}
		if (index >= mLeng) {
			free(data);
			return;
		}
		auto pool = mPool[index];
		if (!pool) {
			auto& conf = pool_items[index];
			pool = new MyDogPoolFixSize(conf.fixsize, conf.releaseCD);
			mPool[index] = pool;
		}
		pool->free(data);
	}
	void MyDogPoolMgr::onLoop(uint32 nt) {
		if (!onesecCd.check(nt)) {
			return;
		}
		for (int i = 0;i< mLeng;i++) {
			auto it = mPool[i];
			if (it && it->checkCd(nt)) {
				it->release(pool_items[i].releasePro);
			}
		}
	}
}