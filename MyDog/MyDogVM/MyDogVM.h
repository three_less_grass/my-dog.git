﻿// MyDogVM.h: 目标的头文件。
#pragma once
#include "MyDogCom.h"

namespace MyDog {
	class ReadBuff;
	class MyDogVM {
	public:
		MyDogVM(ReadBuff& rb);
		~MyDogVM();
		/// <summary>
		/// 运行某个脚本函数
		/// </summary>
		int Run(funcid id, const char* buff, int leng);
	};
}
