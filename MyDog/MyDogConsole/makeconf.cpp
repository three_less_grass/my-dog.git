#include "MyDogConsole.h"
#include "../MyDogVM/MyDogBuff.h"
#include <fstream>

extern map<string, OnFunc> gMapFunc;
extern map<string, string> gMapStr;

using namespace MyDog;

void OnFunc_MDMC_init() {
	WriteDispBuff wb;
	//1 浮点数精度 2 实体最大数量 3 VM配置（暂时为空）
	wb << 10000 << 5000;
	//4 pool配置
	// 其实这些参数，压测一天就有了。。。todo
	// 整体控制在最多10%左右的内碎片浪费的对齐规则;
	// [1,128]					8byte对齐	    10分钟delete一下
	// [128+1,1024]				16byte对齐	    	                 
	// [1024+1,8*1024]			128byte对齐	    
	// [8*1024+1,64*1024]		1024byte对齐     
	// [64*1024+1,256*1024]		8*1024byte对齐
	const PoolConfItem pool_its[] = {
		{16,50,600},
		{24,50,600},
		{32,50,600},
	};

	uint8 pool_leng = sizeof(pool_its) / sizeof(PoolConfItem);
	wb << (uint16)2 << pool_leng;
	/*for (int i = 0;i<pool_leng;i++) {
		wb << pool_its[i];
	}*/
	wb.writeBuff((dptr)&pool_its, sizeof(pool_its));
	
	//生成bin
	uint32 fixs; uint16 useBlock; uint32 leftleng;
	wb.getInfo(fixs, useBlock, leftleng);

	ofstream file("mdconf.bconf", ios::in | ios::trunc);
	for (int i = 0;i< useBlock;i++) {
		int leng = fixs;
		if (i == useBlock - 1) {
			leng = leftleng;
		}
		file.write(wb.readData(i), leng);
	}
	file.close();

}

void InitFuncMakeConf() {
	cout << YELLOW << "注册生成配置的各种函数" << endl;
	RegFunc("mc", OnFunc_MDMC_init, "生成初始化配置");
}