#include "MyDogConsole.h"
#include "../MyDogVM/MyDogStack.h"

extern map<string, OnFunc> gMapFunc;
extern map<string, string> gMapStr;
using namespace MyDog;

void OnFunc_MDStack1() {
	MyDogStack<128> _stack;
	_stack.applyReg(8);
	_stack.applyReg(24);
	_stack.applyReg(48);
	_stack.applyReg(40);
	_stack.applyReg(100);
}
void InitStack() {
	cout << YELLOW << "注册MDStack相关的测试函数" << endl;
	RegFunc("stack1", OnFunc_MDStack1, "MyDogStack的使用");
}