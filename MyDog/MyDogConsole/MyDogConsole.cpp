﻿// MyDogConsole.cpp: 定义应用程序的入口点。
//

#include "MyDogConsole.h"
#include "../MyDogVM/MyDogEntityMgr.h"
#include "../MyDogVM/MyDogVM.h"
#include "../MyDogVM/MyDogBuff.h"
#include "../MyDogVM/MyDogPool.h"
#include "../MyDogVM/MyDogTime.h"
#include "../MyDogVM/MyDogEvent.h"
#include "../MyDogVM/MyDogCom.h"
#include "../MyDogVM/MyDogFace.h"


#include <time.h>
#include <iostream>
#include <sys/timeb.h>

map<string, OnFunc> gMapFunc;
map<string, string> gMapStr;
extern void InitFuncType();
extern void InitFuncMakeConf();
extern void InitStack();

using namespace MyDog;

MYDOG_EXTERN_GLOBAL

namespace MyDog {
	extern stFrame gFrame;
}

unsigned int shortTime(bool sec) {
	if (sec) {
		time_t t;
		time(&t);
		return t;
	}
	else {
		timeb t;
		ftime(&t);//获取毫秒
		return t.time*1000 + t.millitm;
	}
}
int main()
{
	cout << "Hello CMake.输入h,打印所有测试命令" << endl;
	time_t myt = time(NULL);

	gFrame.shortTimeCB = shortTime;
	//初始化全局变量
	const PoolConfItem pool_its = {512,50,600};
	char data[20] = { 0 };
	*(uint8*)data = 1;
	memcpy(data+sizeof(uint8), &pool_its, sizeof(pool_its));
	ReadBuff rb(data);
	gPoolMgr = new MyDogPoolMgr(rb);

	InitFuncType();
	InitFuncMakeConf();
	InitStack();
	string instr;
	while (true) {
		cin >> instr;
		if (instr == "q") {
			break;
		}
		else if (instr == "h") {
			string str = "";
			for (auto it:gMapFunc) {
				str += it.first + ":"+ gMapStr[it.first] + "\n";
			}
			cout << GREEN << "测试命令如下:\n" << str << endl;
			continue;
		}
		auto it = gMapFunc.find(instr);
		if (it != gMapFunc.end()) {
			it->second();
		}
	}
	
	return 0;
}
