#include "MyDogConsole.h"
#include "../MyDogVM/MyDogEntityMgr.h"
#include "../MyDogVM/MyDogVM.h"
#include "../MyDogVM/MyDogType.h"
#include "../MyDogVM/MyDogBuff.h"
#include "../MyDogVM/MyDogPool.h"
#include "../MyDogVM/MyDogTime.h"
#include "../MyDogVM/MyDogEvent.h"
#include "../MyDogVM/MyDogCom.h"


extern map<string, OnFunc> gMapFunc;
extern map<string, string> gMapStr;

using namespace MyDog;


MYDOG_EXTERN_GLOBAL

 void OnFunc_MDfloat1() {

	 gAccuracy = 100;
	 MDfloat f = 0.01;
	 MDfloat f2 = 0.02;
	 auto f3 = f2 * f;
	 cout << f3 << endl;
	 f2 += f;
	 cout << f2 << endl;
 }
 void OnFunc_MDfloat2() {
	 int i = 10;
	 MDfloat f2 = 0.02;
	 f2 += i;
	 cout << (float)f2 << endl;
	 auto f3 = f2 + i;
	 cout << f3 << endl;
	 f3 = i + f2;
	 cout << f3 << endl;
 }
 void OnFunc_MDfloat3() {
	 long long i = 10;
	 MDfloat f2 = 0.02;
	 cout << i * f2 << endl;//=>i*(float)f2
	 cout << f2 * i << endl;//=>MDfloat operator *(const int& rdata)
	 auto f4 = f2 * i;
	 f2 = f4;
	 cout << f2 + i + 10 << endl;
	 int ii = 11;
	 cout << ii * f2 << endl;
	 cout << f2 * ii << endl;
	 auto f3 = i + f2;
	 cout << f3 << endl;
	 f3 = f2 + ii + 10;
	 cout << f3 << endl;
	 f2 = i;
	 cout << f2 << endl;
	 
 }
 void InitFuncType() {
	 cout << YELLOW << "注册MDType相关的测试函数" << endl;
	 RegFunc("mdf1", OnFunc_MDfloat1,"MDfloat间的各种运算");
	 RegFunc("mdf2", OnFunc_MDfloat2, "MDfloat和int的各种运算");
	 RegFunc("mdf3", OnFunc_MDfloat3, "MDfloat和char、int64的各种运算");
 }