﻿using System;
using System.Runtime.InteropServices;
namespace CSharpApp;

public delegate IntPtr ReadCallBack(string file,int reason);
public delegate void WriteCallBack(string file, IntPtr data,int size, int reason);
public delegate uint ShortTimeCallBack(bool bsec);
public delegate void LogCallBack(string log, int type);

// stFrame定义  
[StructLayout(LayoutKind.Sequential)]
public struct StFrame {
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
    public string confFile;
    public ReadCallBack readCB;
    public WriteCallBack writeCB;
    public ShortTimeCallBack shortTimeCB;
    public LogCallBack logCB;
}

/// <summary>
/// 这个类实现C++导出接口IFrame
/// </summary>
public unsafe static class Frame {
    static readonly long startTime = DateTime.Now.Ticks;//开机时间
    public static void InitStFrame(ref StFrame f) {
        f.confFile = "mydog.txt";
        f.readCB = ReadData;
        f.writeCB = WriteData;
        f.shortTimeCB = ShortTimeCallBack;
        f.logCB = Log;
    }
    public static IntPtr ReadData(string filename, int reason) {
        try {
            if (!File.Exists(filename)) {
                return IntPtr.Zero;
            }
            var data = File.ReadAllBytes(filename);
            return Marshal.UnsafeAddrOfPinnedArrayElement(data, 0);
        }
        catch {/** To do: catch code **/return IntPtr.Zero; }
    }
    public static void WriteData(string filename, IntPtr data, int length, int reason) {
        try {
            FileStream file = new(filename, FileMode.Create, FileAccess.Write);
            UnmanagedMemoryStream ustream = new((byte*)data, length);
            ustream.CopyTo(file);
            ustream.Close();
            file.Close();
        }
        catch {/** To do: catch code **/}
    }
    public static uint ShortTimeCallBack(bool bsec) {
        if (bsec) {
            return (uint)(DateTime.Now.Ticks / 10000000);
        } else {
            return (uint)((DateTime.Now.Ticks - startTime) / 10000);
        }
        
    }
    public static void Log(string log, int type) {
        Console.WriteLine(log);
    }
}
