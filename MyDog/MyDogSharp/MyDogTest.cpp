#ifdef MYDOG_TEST
#include <iostream>
#include "MyDogTest.h"
#include "../MyDogVM/MyDogCom.h"
#include "../MyDogVM/MyDogEntityMgr.h"
#include "../MyDogVM/MyDogVM.h"
#include "../MyDogVM/MyDogPool.h"
#include "../MyDogVM/MyDogTime.h"
#include "../MyDogVM/MyDogEvent.h"
#include "../MyDogVM/MyDogType.h"

MYDOG_EXTERN_GLOBAL


using namespace std;
using namespace MyDog;

void OnFunc_MDfloat1() {
	MDfloat f = 0.01;
	MDfloat f2 = 0.02;
	auto f3 = f2 * f;
	cout << f3 << endl;
	f2 += f;
	cout << f2 << endl;
}

/////////////////////////////////华丽的分割线//////////////////////////////

enum ETestType {
	test_base,
	init_main,
	init_thread,//宿主使用多线程的情况,库的thread_local是否有效 
};

void __stdcall Test(int testType) {
	switch (testType) {
	case test_base: {
		gAccuracy = 100;
		OnFunc_MDfloat1();
	}break;
	case init_main: {
		gAccuracy = 100;
	}break;
	case  init_thread: {
		//gEntityMgr = new MyDogEntityMgr();
	}break;
	}
}

#endif
