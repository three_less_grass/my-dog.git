﻿// MyDogSharp.h: 目标的头文件。

#pragma once
#ifndef MYDOG_DLL
#ifdef _WIN32
#if defined(BUILD_DLL)
#define MYDOG_DLL __declspec(dllexport)
#elif defined(USE_DLL)
#define MYDOG_DLL __declspec(dllimport)
#else
#define MYDOG_DLL
#endif // BUILD_DLL
#else
#define MYDOG_DLL
#endif
#endif // !MYDOG_DLL


struct stFrame;
/// <summary>
/// 在逻辑线程初始化
/// </summary>
extern "C" MYDOG_DLL bool __stdcall Init(stFrame& frame);
/// <summary>
/// 宿主框架提供的循环里调用
/// </summary>
extern "C" MYDOG_DLL void __stdcall OnLoop();
/// <summary>
/// 宿主框架退出进程
/// </summary>
extern "C" MYDOG_DLL void __stdcall OnExit();


